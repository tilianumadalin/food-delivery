package com.sda.fooddelivery.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.sda.fooddelivery.hibernate.HibernateUtil;
import com.sda.fooddelivery.model.Restaurant;

public class RestaurantRepository {
	public void saveRestaurant (Restaurant restaurant) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(restaurant);
		
		
		
	    transaction.commit();
	}

}
