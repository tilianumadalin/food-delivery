package com.sda.fooddelivery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.sda.fooddelivery.hibernate.HibernateUtil;
import com.sda.fooddelivery.model.Food;
import com.sda.fooddelivery.model.Restaurant;
import com.sda.fooddelivery.model.User;
import com.sda.fooddelivery.repository.FoodRepository;
import com.sda.fooddelivery.repository.RestaurantRepository;
import com.sda.fooddelivery.repository.UserRepository;

public class App {

	public static void main(String[] args) {
	
	User becali = new User();
	becali.setUsername("gigi_becali");
	becali.setAdress("Pipera_stradaPricipala");
	becali.setTelefon("0771771771");
	becali.setId(01);
	
	
	User borcea = new User();
	borcea.setUsername("cristi_borcea");
	borcea.setAdress("C-ta_Poarta_Alba");
	borcea.setTelefon("0717117111");
	borcea.setId(02);
	
	UserRepository userrepository = new UserRepository();
	userrepository.saveUser(becali);
	userrepository.saveUser(borcea);
	
   Restaurant casaPiratilor = new Restaurant();
   casaPiratilor.setLocatie("Calea Aradului");
   casaPiratilor.setName("Casa Piratilor");
   
   Restaurant vandana = new Restaurant();
   vandana.setLocatie("B-dul Decebal");
   vandana.setName("Vandana");
   
   Restaurant laTurcu = new Restaurant();
   laTurcu.setLocatie("Rogerius");
   laTurcu.setName("La Turcu");
   
   Food pizzaCorsar = new Food();
   pizzaCorsar.setId(12);
   pizzaCorsar.setIngrediente("blat,mozzarella,fructe de mare");
   pizzaCorsar.setName("Pizza Corsar");
   pizzaCorsar.setPret(23.50);
   
   Food pizzaQF = new Food();
   pizzaQF.setId(13);
   pizzaQF.setIngrediente("blat,mozzarella,branzeturi");
   pizzaQF.setName("Pizza Quatro Fromaggi");
   pizzaQF.setPret(24.50);
   
   Food pizzaHawai = new Food();
   pizzaHawai.setId(14);
   pizzaHawai.setIngrediente("blat,mozzarella,ananas");
   pizzaHawai.setName("Pizza Hawai");
   pizzaHawai.setPret(25.00);
   
   Food ciorbaBurta = new Food();
   ciorbaBurta.setId(21);
   ciorbaBurta.setIngrediente("burta vita, gogosari,smantana");
   ciorbaBurta.setName("Ciorba burta");
   ciorbaBurta.setPret(12.35);   
   
   Food ciorbaFasole = new Food();
   ciorbaFasole.setId(22);
   ciorbaFasole.setIngrediente("apa, fasole,cimbru");
   ciorbaFasole.setName("Ciorba fasole");
   ciorbaFasole.setPret(9.15);  
   
   Food orezCiuperci = new Food();
   orezCiuperci.setId(23);
   orezCiuperci.setIngrediente("orez, ciperci");
   orezCiuperci.setName("Orez cu ciuperci");
   orezCiuperci.setPret(12.45);  
   
   Food SnitzelPui = new Food();
   SnitzelPui.setId(24);
   SnitzelPui.setIngrediente("carne pui, ou, pesmet");
   SnitzelPui.setName("Snitel de pui");
   SnitzelPui.setPret(11.73);  
   
   Food ShaormaPui = new Food();
   ShaormaPui.setId(25);
   ShaormaPui.setIngrediente("lipie, carne pui, sosuri, cartofi prajiti");
   ShaormaPui.setName("Shaorma de pui");
   ShaormaPui.setPret(18.00);  
   
   Food ShaormaVitel = new Food();
   ShaormaVitel.setId(26);
   ShaormaVitel.setIngrediente("lipie, carne vitel, sosuri, cartofi prajiti");
   ShaormaVitel.setName("Shaorma de vitel");
   ShaormaVitel.setPret(18.00);  
   
   FoodRepository foodRepository = new FoodRepository();
   foodRepository.saveFood(ShaormaVitel);
   foodRepository.saveFood(ShaormaPui);
   foodRepository.saveFood(SnitzelPui);
   foodRepository.saveFood(orezCiuperci);
   foodRepository.saveFood(ciorbaFasole);
   foodRepository.saveFood(ciorbaBurta);
   foodRepository.saveFood(pizzaHawai);
   foodRepository.saveFood(pizzaQF);
   foodRepository.saveFood(pizzaCorsar);
   
   laTurcu.getFoods().add(ShaormaVitel);
   laTurcu.getFoods().add(ShaormaPui);
   casaPiratilor.getFoods().add(pizzaCorsar);
   casaPiratilor.getFoods().add(pizzaQF);
   casaPiratilor.getFoods().add(pizzaHawai);
   vandana.getFoods().add(orezCiuperci);
   vandana.getFoods().add(SnitzelPui);
   vandana.getFoods().add(ciorbaBurta);
   vandana.getFoods().add(ciorbaFasole);
   
   
   RestaurantRepository restaurantRepository = new RestaurantRepository();
   restaurantRepository.saveRestaurant(casaPiratilor);
   restaurantRepository.saveRestaurant(vandana);
   restaurantRepository.saveRestaurant(laTurcu);
	
   
   
   
	
	}
	
}
    