package com.sda.fooddelivery.hibernate;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import com.sda.fooddelivery.model.Food;
import com.sda.fooddelivery.model.Restaurant;
import com.sda.fooddelivery.model.User;

import java.util.Properties;
public class HibernateUtil {
   // Creates a connection between our java classes
   // and the database table
   private static SessionFactory sessionFactory;
   private static boolean inMemory = false;
   public static SessionFactory getSessionFactory() {
       if(!inMemory) {
           setMySqlSettingToSessionFactory();
       } else {
           setH2SettingsToSessionFactory();
       }
       return sessionFactory;
   }
   private static void setH2SettingsToSessionFactory() {
       if (sessionFactory == null) {
           try {
               Configuration configuration = new Configuration();
               Properties settings = new Properties();
               settings.put(Environment.URL, "jdbc:h2:mem:myDb;DB_CLOSE_DELAY=-1");
               //settings.put(Environment.USER, "root");
               //settings.put(Environment.PASS, "root0000");
               settings.put(Environment.DRIVER, "org.h2.Driver");
               settings.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
               settings.put(Environment.SHOW_SQL, "true");
               settings.put(Environment.HBM2DDL_AUTO, "create");
               configuration.setProperties(settings);
               configuration.addAnnotatedClass(User.class);
               
               sessionFactory = configuration.buildSessionFactory();
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
   }
   private  static void setMySqlSettingToSessionFactory() {
       if (sessionFactory == null) {
           try {
               Configuration configuration = new Configuration();
               Properties settings = new Properties();
               settings.put(Environment.URL, "jdbc:mysql://localhost:3306/practical_project_day1?serverTimezone=UTC");
               settings.put(Environment.USER, "root");
               settings.put(Environment.PASS, "root0000");
               settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
               settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
               settings.put(Environment.SHOW_SQL, "true");
               settings.put(Environment.HBM2DDL_AUTO, "create");
               configuration.setProperties(settings);
               configuration.addAnnotatedClass(User.class);
               configuration.addAnnotatedClass(Restaurant.class);
               configuration.addAnnotatedClass(Food.class);

               
               sessionFactory = configuration.buildSessionFactory();
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
   }
   public static void setInMemory(boolean inMemory) {
       HibernateUtil.inMemory = inMemory;
   }
}