package com.sda.fooddelivery.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.sda.fooddelivery.hibernate.HibernateUtil;
import com.sda.fooddelivery.model.Food;

public class FoodRepository {
	public void saveFood (Food food) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.save(food);
		
		
		
	    transaction.commit();

   }
}
